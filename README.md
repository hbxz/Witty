# Witty public

> 这里目前作为项目公共 wiki。


## 数据流向：

Authoring tools 作为网页应用，课程负责人编辑和维护。（后台导出每一节课相对应的 Narrative.json ）

客户端 访问某一课程时，网络请求 Narrative.json 并加载，然后UI层表现出来。


## 开发/产品的假设：

1. 数据量上：对话不会超过1000节，一节不会超过1000消息
2. 数据的结构上：剧情走向以收敛为主，不会是极端分散的（也就是不是个平衡二叉树）
3. step 的 nexts 里边建议不要超过两个（两个就二叉树，三个就是三叉）




> 暂时 Settings-General-Visibility Leve: public. 酱紫参与者无需注册就可以看内容。等咱们项目做大一丢丢，就改为 private。